let boxContainer = document.querySelector(".box-container");

const createBox = (imagePath, imageName, boxCount) => {
  let box = document.createElement("div");
  let blackBox = document.createElement("div");
  let image = document.createElement("img");

  box.classList.add("black-box-container");
  box.setAttribute("data-image", imageName);
  box.setAttribute("data-count", boxCount);
  blackBox.classList.add("box");

  image.classList.add("image");
  image.setAttribute("src", imagePath);

  box.appendChild(image);
  box.appendChild(blackBox);
  boxContainer.appendChild(box);
};

let imageNameArray = [
  "apple.jpg",
  "brinjal.png",
  "kiwi.jpg",
  "mango.jpg",
  "pineapple.png",
  "water-melon.webp",
  "banana.webp",
  "pomegranate.jpeg",
];

function boxesCreation() {
  let count = 7;
  let imagePath;
  let imageName;
  for (let boxCount = 0; boxCount < 16; boxCount++) {
    if (boxCount < 8) {
      imagePath = `./images/${imageNameArray[boxCount]}`;
      imageName = imageNameArray[boxCount];
    } else {
      imagePath = `./images/${imageNameArray[count]}`;
      imageName = imageNameArray[count];
      count--;
    }
    createBox(imagePath, imageName, boxCount);
  }
}

boxesCreation();

let startButton = document.querySelector(".start-button");
let time = document.querySelector("#time");
let moveDiv = document.querySelector("#move");
let score = document.querySelector("#your-score");
let blackBoxContainers = document.querySelectorAll(".black-box-container");

let gameTimer;
let isStartButtonClicked = false;
let moves = 0;
let matchScore = 0;
let hasFlippedCard = false;
let firstCard, secondCard;

function undoFlipping() {
  blackBoxContainers.forEach((box) => {
    box.classList.remove("flip");
    box.addEventListener("click", flipBlackBox);
  });
}

function resetGameInformation() {
  moves = 0;
  moveDiv.textContent = "0 moves";
  matchScore = 0;
  score.textContent = "Your Score: 0";
  undoFlipping();
}

function startTimer() {
  let second = 0;
  time.textContent = `time: ${second} s`;
  gameTimer = setInterval(() => {
    time.textContent = `time: ${++second} s`;
  }, 1000);
}

function stopTimer() {
  clearInterval(gameTimer);
}

function startGame() {
  if (isStartButtonClicked) {
    isStartButtonClicked = false;
    startButton.innerText = "Start";
    startButton.style.backgroundColor = "black";
    stopTimer();
  } else {
    isStartButtonClicked = true;
    startButton.innerText = "Stop";
    startButton.style.backgroundColor = "red";
    resetGameInformation();
    startTimer();
  }
}

startButton.addEventListener("click", startGame);

function flipBlackBox() {
  if (!isStartButtonClicked) {
    return;
  }

  this.classList.add("flip");
  countMoves();

  if (!hasFlippedCard) {
    hasFlippedCard = true;
    firstCard = this;
  } else {
    hasFlippedCard = false;
    secondCard = this;

    checkForMatch(firstCard, secondCard);
  }
}

function countMoves() {
  moves++;
  moveDiv.textContent = `${moves} moves`;
}

function checkForMatch(firstCard, secondCard) {
  if (firstCard.dataset.image === secondCard.dataset.image) {
    if (firstCard.dataset.count === secondCard.dataset.count)
      flipBackInstant(firstCard);
    else displayCards(firstCard, secondCard);
  } else {
    flipBack(firstCard, secondCard);
  }
}

function displayCards(firstCard, secondCard) {
  firstCard.removeEventListener("click", flipBlackBox);
  secondCard.removeEventListener("click", flipBlackBox);
  setScore();
}

function setScore() {
  matchScore++;
  score.textContent = `Your Score: ${matchScore}`;
  if (matchScore === 8) stopTimer();
}

function flipBackInstant(firstCard) {
  firstCard.classList.remove("flip");
}

function flipBack(firstCard, secondCard) {
  setTimeout(() => {
    firstCard.classList.remove("flip");
    secondCard.classList.remove("flip");
  }, 1000);
}

blackBoxContainers.forEach((blackBox) =>
  blackBox.addEventListener("click", flipBlackBox)
);
